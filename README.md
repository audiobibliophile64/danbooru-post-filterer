# danbooru-post-filterer [![License](https://img.shields.io/badge/license-MIT-blue?style=flat-square)](LICENSE)

Filters post metadata from the [Danbooru2021 dataset][db21] quickly and efficiently. Can be used to select data to train or fine tune machine learning models with.

## Usage

```sh
danbooru-post-filterer --exclude-tags-file ./banned_tags.txt --include-tags-file ./wanted_tags.txt -r safe posts*.json > filtered_posts.json
```

The program takes a list of file paths to read posts from (specified above using the glob `posts*.json`, which is expanded by the shell before being passed as arguments to the program). It then outputs to stdout only those posts that pass the specified filters. It also logs info to stderr, the verbosity of which can be increased using the `-v` argument. In the command above, the output from stdout is redirected into the `filtered_posts.json` file by the shell.

The exclude tags and include tags filters are OR-based. When both options are specified, posts are filtered first to remove posts that have any tag in the _exclude tags list_, then to remove posts that _don't_ have any tag in the _include tags list_.

The format of the include/exclude tags files is one tag per line. The order of the tags does not matter.

## Installation

The program is cross-platform and should run without issue on Linux, Mac and Windows (including WSL and Cygwin).

### Install from source

1. Make sure you have rust installed https://www.rust-lang.org/tools/install
2. Install the program from this git repo using `cargo`:
    ```sh
    cargo install --git https://gitlab.com/audiobibliophile64/danbooru-post-filterer.git
    ```
3. That's it! You should now be able to run `danbooru-post-filterer -h` to display the usage documentation.

## Obtaining the data

This program is intended to be used on the posts metadata json files from the [Danbooru2021 dataset][db21]. The relevant files can be downloaded from their `rsync` server using this command:

```sh
rsync rsync://176.9.41.242:873/danbooru2021/metadata/posts*.json ./metadata/
```

It may also be helpful to download the file containing all tags:

```sh
rsync rsync://176.9.41.242:873/danbooru2021/metadata/tags000000000000.json ./metadata/
```

The following shell script takes the `tags000000000000.json` file and outputs only tags with a minimum post usage count, sorted by usage in separate files per tag category (general, artist, copyright, character, meta):

```sh
#!/usr/bin/env bash
set -euo pipefail

tags_file="tags000000000000.json"

function get_tags() {
    local category=$1
    local min_count=$2
    jq --slurp -r --arg category "$category" --arg min_count "$min_count" 'map((.post_count = (.post_count | tonumber)) | select(.post_count >= ($min_count | tonumber) and .category == $category)) | sort_by(.post_count) | reverse | map(.name + " " + (.post_count | tostring)) | .[]'
}

# Adjust min_count arguments as desired
< "$tags_file" get_tags 0 100 > tags_general.txt
< "$tags_file" get_tags 1 3 > tags_artist.txt
< "$tags_file" get_tags 3 3 > tags_copyright.txt
< "$tags_file" get_tags 4 3 > tags_character.txt
< "$tags_file" get_tags 5 1 > tags_meta.txt
```

It uses [`jq`](https://stedolan.github.io/jq/), so you'll need to have that installed.

[db21]: https://www.gwern.net/Danbooru2021
