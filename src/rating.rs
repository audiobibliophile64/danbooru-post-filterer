use clap::ValueEnum;

/// Ratings as per Danbooru2021 dataset
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, ValueEnum)]
pub enum Rating {
    Safe,
    Questionable,
    Explicit,
}

impl Rating {
    pub fn to_dataset_repr(&self) -> &'static str {
        match self {
            Rating::Safe => "s",
            Rating::Questionable => "q",
            Rating::Explicit => "e",
        }
    }
}
