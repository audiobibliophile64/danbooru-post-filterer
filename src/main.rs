use ahash::AHashSet;
use clap::Parser;
use danbooru_post_filterer::{combining_method::CombiningMethod, rating::Rating};
use log::LevelFilter;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

#[derive(Parser)]
#[clap(version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Cli {
    /// Makes logging more verbose. Pass once for debug log level, twice for trace log level.
    #[clap(short, parse(from_occurrences), global = true)]
    verbose: u8,
    /// Tags in this file will be used to filter posts. Only posts NOT containing any of the tags will be output.
    #[clap(long)]
    exclude_tags_file: Option<String>,
    /// Tags in this file will be used to filter posts. Only posts containing any/all of the tags will be output.
    #[clap(long)]
    include_tags_file: Option<String>,
    /// Only posts NOT containing any of these tags will be output.
    #[clap(long, short = 'e')]
    exclude_tags: Vec<String>,
    /// Only posts containing any/all of these tags will be output.
    #[clap(long, short = 'i')]
    include_tags: Vec<String>,
    /// Only posts with one of these ratings will be output.
    #[clap(long, short = 'r', value_enum)]
    include_ratings: Vec<Rating>,
    /// The method by which multiple include tags are combined.
    /// or = the post passes the filter if it contains any tag.
    /// and = the post passes the filter if it contains all tags.
    #[clap(long, value_enum, default_value_t = CombiningMethod::Or)]
    combine_include_tags: CombiningMethod,
    /// Whether to exclude posts with is_banned flag set to true from output.
    #[clap(long, default_value_t = true)]
    exclude_banned: bool,
    /// The posts metadata json file(s) to read.
    files: Vec<String>,
}

fn read_lines_to_hashset<P>(path: P) -> Result<AHashSet<String>, anyhow::Error>
where
    P: AsRef<Path>,
{
    let file = File::open(path)?;
    let buf = BufReader::new(file);
    let lines = buf
        .lines()
        .map(|l| l.expect("Failed to read line").trim().to_string())
        .collect::<AHashSet<String>>();
    Ok(lines)
}

fn main() -> Result<(), anyhow::Error> {
    let cli = Cli::parse();

    env_logger::Builder::new()
        .filter_level(match cli.verbose {
            0 => LevelFilter::Info,
            1 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        })
        .init();

    let mut include_tags = cli
        .include_tags_file
        .as_ref()
        .map(read_lines_to_hashset)
        .transpose()?
        .unwrap_or_default();

    include_tags.extend(cli.include_tags);

    let mut exclude_tags = cli
        .exclude_tags_file
        .as_ref()
        .map(read_lines_to_hashset)
        .transpose()?
        .unwrap_or_default();

    exclude_tags.extend(cli.exclude_tags);

    danbooru_post_filterer::lets_filter_us_some_posts(
        cli.files,
        cli.include_ratings.into_iter().collect::<AHashSet<_>>(),
        include_tags,
        exclude_tags,
        cli.combine_include_tags,
        cli.exclude_banned,
    )?;

    Ok(())
}
