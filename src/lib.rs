use ahash::AHashSet;
use anyhow::Context;
use combining_method::CombiningMethod;
use itertools::Itertools;
use rating::Rating;
use regex::{RegexSet, RegexSetBuilder};
use serde::Deserialize;
use std::{
    fs::File,
    io::{BufRead, BufReader, Write},
};

pub mod combining_method;
pub mod rating;

#[derive(Deserialize, Debug)]
struct Post {
    rating: String,
    tag_string: String,
    is_banned: bool,
}

fn should_keep_post(
    post: &Post,
    include_ratings: &[&'static str],
    include_tags_regex_set: &Option<RegexSet>,
    exclude_tags_regex_set: &Option<RegexSet>,
    combine_include_tags: &CombiningMethod,
    exclude_banned: bool,
) -> bool {
    (!(exclude_banned && post.is_banned))
        && (include_ratings.is_empty() || include_ratings.iter().any(|r| *r == post.rating))
        && match include_tags_regex_set {
            Some(r) => {
                let mut split = post.tag_string.split(' ').filter(|s| !s.is_empty());

                match combine_include_tags {
                    CombiningMethod::Or => split.any(|tag| r.is_match(tag)),
                    CombiningMethod::And => {
                        let mut did_match = vec![false; r.len()];
                        for tag in split {
                            for matched_index in r.matches(tag).iter() {
                                did_match[matched_index] = true;
                            }
                        }
                        // After checking all the post's tags, every regex in the set should have
                        // had a match at least once.
                        did_match.into_iter().all(|m| m)
                    }
                }
            }
            None => true,
        }
        && match exclude_tags_regex_set {
            Some(r) => !post
                .tag_string
                .split(' ')
                .filter(|s| !s.is_empty())
                .any(|tag| r.is_match(tag)),
            None => true,
        }
}

pub fn lets_filter_us_some_posts(
    files: Vec<String>,
    include_ratings: AHashSet<Rating>,
    include_tags: AHashSet<String>,
    exclude_tags: AHashSet<String>,
    combine_include_tags: CombiningMethod,
    exclude_banned: bool,
) -> Result<(), anyhow::Error> {
    log::info!("Will process {} files", files.len());
    log::debug!("Files to process: {:?}", files);
    log::debug!("Ratings to include: {:?}", include_ratings);
    log::debug!("Tags to include: {:?}", include_tags);
    log::debug!("Tags to exclude: {:?}", exclude_tags);

    // Open handles to all files ahead of time
    let files = files
        .into_iter()
        .map(|p| File::open(&p).map(|f| (f, p)))
        .collect::<Result<Vec<_>, _>>()?;

    let include_tags_regex_set = match include_tags.is_empty() {
        true => None,
        false => Some(
            RegexSetBuilder::new(
                include_tags
                    .iter()
                    .sorted()
                    .map(|s| format!("^{}$", regex::escape(s))),
            )
            .case_insensitive(true)
            .build()
            .context("error creating include tags regex set")?,
        ),
    };

    log::trace!(
        "Regex set for tags to include: {:?}",
        include_tags_regex_set
    );

    let exclude_tags_regex_set = match exclude_tags.is_empty() {
        true => None,
        false => Some(
            RegexSetBuilder::new(
                exclude_tags
                    .iter()
                    .sorted()
                    .map(|s| format!("^{}$", regex::escape(s))),
            )
            .case_insensitive(true)
            .build()
            .context("error creating exclude tags regex set")?,
        ),
    };

    log::trace!(
        "Regex set for tags to exclude: {:?}",
        exclude_tags_regex_set
    );

    let include_ratings = include_ratings
        .into_iter()
        .map(|r| r.to_dataset_repr())
        .collect::<Vec<_>>();

    for (file, path) in files.into_iter() {
        log::info!("Processing file {}", &path);

        let reader = BufReader::new(file);

        let stdout = std::io::stdout();
        let mut stdout_handle = stdout.lock();

        let mut pass_count = 0u64;
        let mut fail_count = 0u64;
        let mut error_count = 0u64;
        for (index, line) in reader.lines().enumerate() {
            let line = match line {
                Ok(l) => l,
                Err(error) => {
                    log::error!("Error reading line {} of {}: {:?}", index + 1, &path, error);
                    error_count += 1;
                    continue;
                }
            };

            let post: Post = match serde_json::from_str(&line) {
                Ok(post) => post,
                Err(error) => {
                    log::error!(
                        "Error parsing post JSON on line {} of {}: {:?}",
                        index + 1,
                        &path,
                        error
                    );
                    error_count += 1;
                    continue;
                }
            };

            if !should_keep_post(
                &post,
                &include_ratings,
                &include_tags_regex_set,
                &exclude_tags_regex_set,
                &combine_include_tags,
                exclude_banned,
            ) {
                fail_count += 1;
                continue;
            }

            pass_count += 1;
            let mut line = line;
            line += "\n";
            stdout_handle
                .write_all(line.as_bytes())
                .expect("writing line to stdout should not fail");
        }

        let total_count = pass_count + fail_count + error_count;

        log::info!("Done processing file {}", &path);
        log::info!("Passed posts: {}", &pass_count);
        log::info!("Failed posts: {}", &fail_count);
        log::info!("Errored posts: {}", &error_count);
        log::info!("Total posts: {}", &total_count);
    }

    log::info!("Done processing all files");

    Ok(())
}
